After proceeding with the upload process, the only thing left to do, is to add the Web Part to one or more channels. <br>
To do this, please follow the steps below: 

1. To start using the Web Part, please select your desired **channel**;

1. Click on the plus symbol (+) to add a new **Tab**; 

1. Choose the desired Web Part, from the available pool;

    ![available-pool.png](../../images/available-pool.png)

1. Click **Save**.