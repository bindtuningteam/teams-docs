
### Upload to Teams

In order to proceed with the installation, you'll have to verify your organization policies regarding the upload of **Custom apps**, namely who and where the solutions can be applied. 

**Note:** In order to modify these policies, permissions of either **global admin** or **Teams service admins** are required.

An overview on **Custom App Policies and Microsoft Teams** can be found <a href="https://docs.microsoft.com/en-us/microsoftteams/teams-custom-app-policies-and-settings#how-custom-app-policies-and-settings-work-together" target="_blank">here</a>.

After verifying the correct policies are in place, the installation process of **Custom apps** can proceed in three different ways: 

- Upload to a particular **Tab**; 
- Upload to a particular **Team**; 
- Upload the solution **globally**, thus making it available for the entire organization.

___
#### Upload to a Tab

This installation process will, automatically, deploy the solution to a new **tab** in the specified **channel**. However, note that it will also make the solution available to the specific **team** it was deployed in. 

1. At the bottom left of your Teams application, click on **Apps**; 

    ![apps-icon](../../images/apps-icon.png)

1. Click the option **Upload a custom app**;

    ![upload-custom-app.gif](../../images/upload-custom-app.gif)

1. Select the Web Part package you want to install (*WebPartName MSTeams.zip*);

    ![select-zip.gif](../../images/select-zip.gif)

1. After uploading the solution (*.zip* file), click on **Add to a team**;

    ![add-to-teams.png](../../images/add-to-teams.png)

1. After adding the solution, you'll have to choose the channel you want to add the Web Part to; 
    
    ![choose-channel.png](../../images/choose-channel.png)

1. To finalize the process, click on **Save**.

    ![save-tab.png](../../images/save-tab.png)

The Web Part is fully available on your Team. ✅

___ 
#### Upload to a Team

This installation process will make the solution available for the entire **Team**, where it was deployed in. 

To upload to a **Team**, follow the steps below: 

1. Locate the **Team** you want to deploy the solution in; 

    ![locate-team.png](../../images/locate-team.png)

1. Click on the **three dots** and select **Manage Team**; 

1. Navigate to the **Apps** tab; 

    ![apps-tab.png](../../images/apps-tab.png)

1. Select the option **Upload a custom app**; 

1. Select the Web Part package you want to install (*WebPartName MSTeams.zip*);

    ![upload-custom-app-team](../../images/upload-custom-app-team.gif)

1. After uploading the solution (*.zip* file), click on **Add**;

The Web Part is fully available on your Team. ✅

___
#### Global upload

This installation process will make the solution available for the entirety of your **tenant**. This means that it can be used on any **Team** belonging to your organization. 

**Note**: This deployment method will require a permission level of, at least, **Teams service admin**.

To upload the solution globally, please follow the steps below: 

1. Log in to your **Microsoft Teams** account; 

1. At the bottom left of your Teams application, click on **Apps**; 

    ![apps-icon](../../images/apps-icon.png)

1. Select the option **Upload a custom app**, followed by **Upload for [TENANT]**; 

    ![upload-for-tenant.gif](../../images/upload-for-tenant.gif)

1. The Web Part will be added automatically. 

    ![tenant-added-app.png](../../images/tenant-added-app.png)

The Web Part is fully available for your orgnaization. ✅
