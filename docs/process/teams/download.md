
Ready to install your BindTuning Web Parts on Microsoft Teams?

**Note:** If you have deployed the Web Parts on your SharePoint environment manually, move to the following [section](../../process/teams/installation.md).

1. Sign in to your <a href="https://bindtuning.com" target="_blank">BindTuning</a> account;

1. Go to **Build** feature tab.

1. Mouse over the intended Web part and click on **More Details**.

    ![manual-install-1.png](../../images/manual-install-1.png)

4. Last but not least, click on **Download**.

    ![Download.PNG](../../images/manual-download.png)

