The BindTuning online app allows for the easy deployment of Web Parts on Office 365 (SharePoint Online).

**Note:** This installation method is only valid for Office 365 (SharePoint Online).

If you need to make the Teams Add-On functionality **available to all your Teams**, proceed with the [Tenant App Catalog Installation](#install-web-parts-on-tenant-app-catalog); if you want to restrict usage to a **subset of your Teams**, proceed with the [Site Collection App Catalog Installation](#install-web-parts-on-site-collection-app-catalog).
___
### Install Web Parts on Tenant App Catalog

1. Login to your BindTuning account; 

1. Navigate to the **Build** tab; 

    ![build-feature](../../../images/build-feature.png)

1. Mouse over the selected Web Part and click on **More Details**;

1. Click on **Install**; 

1. Proceed with either: 

    - The installation of the selected Web Part; 
    - A bulk installation of all Web Parts.

    ![install-online-app](../../../images/install-online-app.png)

1. Select the option **Office 365**; 

1. Input your corresponding Office 365 user email; 

1. Choose the **Tenant** deployment scope; 

1. Verify all the provided information and click **Confirm**. 

    ![tenant-deployment](../../../images/tenant-deployment.png)

1. The installation will proceed in the background. To review it's progress click on **Recent Activity** (graph icon).

After the installation has been completed, an alert will appear, informing you of it's status.

___
### Install Web Parts on Site Collection App Catalog

1. Login to your BindTuning account; 

1. Navigate to the **Build** tab; 

    ![build-feature](../../../images/build-feature.png)

1. Mouse over the selected Web Part and click on **More Details**;

1. Click on **Install**; 

1. Proceed with either: 

    - The installation of the selected Web Part; 
    - A bulk installation of all Web Parts.

    ![install-online-app](../../../images/install-online-app.png)

1. Select the option **Office 365**; 

1. Input your corresponding Office 365 user email; 

1. Choose the **Site Collection** deployment scope; 

1. Select the site you wish to deploy the product to and hit **Next**; 

    ![select-site](../../../images/select-site.png)

1. Verify all the provided information and click **Confirm**. 

    ![tenant-deployment](../../../images/site-collection-deployment.png)

1. The installation will proceed in the background. To review it's progress click on **Recent Activity** (graph icon).

After the installation has been completed, an alert will appear, informing you of it's status.