If you need to make the Teams Add-On functionality **available to all your Teams**, proceed with the [Tenant App Catalog Installation](#install-web-parts-on-tenant-app-catalog); if you want to restrict usage to a **subset of your Teams**, proceed with the [Site Collection App Catalog Installation](#install-web-parts-on-site-collection-app-catalog).

___
### Install Web Parts on Tenant App Catalog

To add our Teams Add-on, you'll have to upload the Web Part package to the Tenant App Catalog. To do this, follow the steps below: 

1. Open the **Office 365** app launcher and click on **Admin**;

	![Admin Center.png](https://bitbucket.org/repo/daEeqRX/images/1075500318-Admin%20Center.png)
	
1. Open your SharePoint **Admin Center**; 

	![open-sharepoint-admin-center.png](../../../images/open-sharepoint-admin-center.png)

1. On the left panel, click on **More features** and select **Apps**;

	![open-app-catalog.png](../../../images/open-app-catalog.png)
	
1. Proceed by clicking on **App Catalog**;

1. On the left pane, click on **Apps for SharePoint**;

	![APPs for SharePoint.png](https://bitbucket.org/repo/6499g4B/images/2864249072-APPs%20for%20SharePoint.png)
    
1. Now click on **Upload** and select the *.sppkg* file that's inside the **spfx** folder in your web part package.

    ![Install SPFx.gif](https://bitbucket.org/repo/daEeqRX/images/2122301107-Install%20SPFx.gif)
    
    
1. Finally, on the pop up that appears, tick the checkbox **Make this solution available to all sites in the organization** and click **Deploy**;

    ![Trust and deploy.png](https://bitbucket.org/repo/6499g4B/images/2615707783-Trust%20and%20deploy.png)

___
### Install Web Parts on Site Collection App Catalog

To add our Teams Add-on, you'll have to upload the Web Part package to the Site Collection App Catalog. To do this, follow the steps below: 

1. Open the Site Collection you wish to deploy the product to;
	
1. Click on **Settings** and navigate to your **Site Contents**; 

1. Open your **Apps for SharePoint** list;

    **Note:** If you're unable to see the **Apps for SharePoint** list, this means that you do not have a **Site Collection App Catalog** created. To do so, follow the steps described in the following [article](https://support.bindtuning.com/hc/en-us/articles/360027696532-What-is-and-How-to-create-an-App-Catalog-Tenant-vs-Site-Collection-App-Catalog).

1. On the left pane, click on **Apps for SharePoint**;

	![APPs for SharePoint.png](https://bitbucket.org/repo/6499g4B/images/2864249072-APPs%20for%20SharePoint.png)
    
1. Now click on **Upload** and select the *.sppkg* file that's inside the **spfx** folder in your web part package.

    ![Install SPFx.gif](https://bitbucket.org/repo/daEeqRX/images/2122301107-Install%20SPFx.gif)
    
    
1. Finally, on the pop up that appears, tick the checkbox **Make this solution available to all sites in the organization** and click **Deploy**;

    ![Trust and deploy.png](https://bitbucket.org/repo/6499g4B/images/2615707783-Trust%20and%20deploy.png)


