After downloading, you'll have to unzip the contents of the 
Web Part folder.
<br>

Each Web Part will contain the following structure: 

![folder-structure.png](../../../images/folder-structure.png)

To install the **Teams add-on**, we will need the contents inside the **spfx** folder:

── **spfx** 📁<br>
└── Office365<br>
└── SP2019<br>

Click on **Office365** and inside, you'll see a zipped folder containing the **Teams add-on**, that will allow for the installation inside Microsoft Teams. 

![zipped-folder.png](../../../images/zipped-folder.png)

**Note:** The contents inside the **BT_*webpartname* MSTeams.zip** should not be extracted, as the installation will require the zipped solution to be uploaded. 

