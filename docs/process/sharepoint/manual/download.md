**Note:** The first step in installing BindTuning's Teams Add-on will, always, consist in the upload of the Web Parts to your **Tenant and/or Site Collection App Catalog**.

In this guide, you'll find how to **Download**, **Unzip** and **Install** the solutions to the **app catalog**, before proceeding to **upload** them to Microsoft Teams. 
___
Ready to install your BindTuning Web Parts on Microsoft Teams?

1. Sign in to your <a href="https://bindtuning.com" target="_blank">BindTuning</a> account;

1. Go to **Build** feature tab.

1. Mouse over the intended Web part and click on **More Details**.

    ![manual-install-1.png](../../../images/manual-install-1.png)

4. Last but not least, click on **Download**.

    ![Download.PNG](../../../images/manual-download.png)

