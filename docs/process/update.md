Since the Web Part bundle offered by BindTuning releases new features and/or improvements with each version, you'll have to update the Web Parts so as to have the best experience. 
<br>

To update the Web Parts installed on your Teams channel, you need to update them on your BindTuning account.

---
### Update Web Parts on Tenant App Catalog

1. Login to your **BindTuning account** at <a href="www.bindtuning.com">BindTuning</a>;
2. Navigate to your **Build** tab;
3. If an update for the Web Part(s) is available, you'll be able to see a red circle.

    ![available-updates.png](../images/available-updates.png)

4. To update the Web Parts, click on the icon and select **Update Now**;

    ![update-one-webpart.png](../images/update-one-webpart.png)

5. The update process will begin and you'll be notified once the update has finished. 

6. After the newest version has finished generating, you'll be able to <a href="https://bindtuning-teams.readthedocs.io/en/latest/process/sharepoint/manual/download/">download</a> and <a href="https://bindtuning-teams.readthedocs.io/en/latest/process/sharepoint/manual/installation/">re-upload</a> the Web Parts on Microsoft Teams. 

7. The update process has been completed! ✅

---
### Update Web Parts on Teams

1. Open you **Teams Admin Center**; 

1. On the left panel, select **Teams apps**, followed by **Manage Apps**; 

1. Locate the **BindTuning Web Parts** you wish to update;

    ![locate-bindtuning-apps.gif](../images/locate-bindtuning-apps.gif)

1. Click on the specific Web Part; 

1. Select the **Update** option; 

1. Click on **Select a file**

    ![select-file](../images/select-file.png)

1. Upload the corresponding **.zip** file.

    ![upload-updated-zip](../images/upload-updated-zip.gif)

The Web Part is fully updated! ✅


