### Uninstall from Team

To uninstall BindTuning solutions from a specific Team: 

1. Locate the **Team** where you have deployed the solutions; 
1. Click on the 3 dots, followed by **Manage Team**; 
1. Navigate to **Apps**; 

    ![uninstall-custom-apps](../images/unistall-custom-apps.gif)

1. Select the BindTuning solution you want to uninstall; 
1. Click on the **trash** icon; 

    ![uninstall-button.png](../images/uninstall-button.png)

1. Confirm by clicking on **Uninstall**. 

    ![uninstall-prompt.png](../images/uninstall-prompt.png)

---
### Global Uninstallation

To globally uninstall BindTuning solutions from your Microsoft Teams, refer to the steps below: 

1. Open you **Teams Admin Center**; 

1. On the left panel, select **Teams apps**, followed by **Manage Apps**; 

1. Locate the **BindTuning Web Parts** you wish to uninstall;

    ![locate-bindtuning-apps.gif](../images/locate-bindtuning-apps.gif)

1. Click on the specific Web Part; 

1. Select the **Delete** option; 

1. Confirm by pressing **Delete**. 

    ![delete-from-teams](../images/delete-from-teams.png)

