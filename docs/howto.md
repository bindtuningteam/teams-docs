
### Install in SharePoint 

The installation of the Teams Add-On is inherently dependent on the prior addition of the Build feature (Web Parts) to your **SharePoint App Catalog**. 

With this in mind, we're able to perform the installation at two distinct scopes: 

- **Install Web Parts on the Tenant App Catalog** - Ensuring the Web Parts will be available on all new and pre-existing site collections, hence making sure you're able to **add your Teams Add-On to any Team inside your organization**. 

- **Install Web Parts on the Site Collection App Catalog** - Restricting Web Part usage to the site collection in question. 

    **Note:** If you deploy the Web Parts (Build) in a specific Team site (created automatically after creating a Microsoft Teams Team), the Teams Add-On will only be available to be added on that same Team.


<br>
BindTuning provides you with the flexibility to install our products the way you want to, regardless of technical knowledge or experience.

Following this, BindTuning offers two main installation processes:

- **Automated** installation - making the deployment of BindTuning's products as easy as a simple click. ;
- **Manual** installation - allowing for more granular control on the installation with an added complexity layer, not present in the Automated installation.

**Note:** We recommend the **Automated** installation, due to its easy deployment process.

---
### Install in Microsoft Teams

After the successful deployment of the product on your SharePoint environment, you'll need to add the product to your Microsoft Teams environment. 

This procedure is only able to be performed **manually**.

Alternatively, BindTuning provides a set of templates, able to be deployed via the **Automated installation** method. Those same templates contain a Microsoft Teams Team, coupled with BindTuning's Teams Add-On and demo content. Further information relating to this feature can be found <a href="https://bindtuning.com/starter-kits" target="_blank" rel="noopener">here</a>.

**Note:** Deployment of BindTuning's Microsoft Teams Starter Kits will install the product at the **Site Collection** level. If you wish to deploy the product globally proceed with the **manual installation** process.




